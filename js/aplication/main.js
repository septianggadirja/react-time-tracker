var react = require('react');
var bootstrap = require('react-bootstrap');
var router = require('react-router');
var header = require('./views/header.js');
var Sidebar = require('./views/sidebar.js');
var mainSection = require('./views/main_section.js');
var Route = Router.Route, DefaultRoute = Router.DefaultRoute;

header(bootstrap, react, document.getElementById('header'));
mainSection(bootstrap, react, router, document.getElementById('mainsection'));

var routes = (
  	<Route name="Mainsection" path="/" handler={Mainsection}>
    <Route name="Dashboard" handler={Dashboard}/>
    <DefaultRoute handler={Dashboard}/>
  </Route>
);

  Router.run(routes, function (Handler) {
  	React.render(<Handler/>,  domLocation );
  });
