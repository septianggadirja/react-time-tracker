//var description
var mountNode = document.getElementById('main-content')

var Route = ReactRouter.Route;
var DefaultRoute = ReactRouter.DefaultRoute;
var Link = ReactRouter.Link;
var RouteHandler = ReactRouter.RouteHandler;


//var bootstrap
var Grid = ReactBootstrap.Grid;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;
var ModalTrigger = ReactBootstrap.ModalTrigger;
var Button = ReactBootstrap.Button;
var Modal = ReactBootstrap.Modal;
var OverlayMixin = ReactBootstrap.OverlayMixin;



//get ListProject -- untuk content dari listing project
var ListProject = React.createClass({
  render: function() {
    return (
      <p>
	  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
      consequat.
	  </p>
    );
  }
});

//project komponent
var Project_id = React.createClass({
  render: function() {
    return (
      <div className="project">
	  	<h2>Urgent Prototyping</h2>
            <ListProject />
            <div className="user-activity">
               <img src={'img/user-profile.jpg'} />
               <img src={'img/user-profile.jpg'} />
            </div>
            <div className="user-time">
               <h3>Estimated time</h3>
               <h2>24 hours</h2>
            </div>
      </div>
    );
  }
});


// all header component============================
//logo
var Logo = React.createClass({
  render: function() {
    return (
      <div className="logo-name">
	  	<div className="logo-top">
	  		<a href="">Time tracker</a>
      	</div>
      </div>
    );
  }
});

//user image (ini ane belum faham cara ambil data image ak asep maknya statis dulu heheheh)
var UserImage = React.createClass({
  render: function() {
    return (
      <div className="img-user">
	  		<img src={'img/user-profile.jpg'} /> 
			
      </div>
    );
  }
});

// modal add task on header
var CustomModalTrigger = React.createClass({
  mixins: [OverlayMixin],

  getInitialState: function () {
    return {
      isModalOpen: false
    };
  },

  handleToggle: function () {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  },

  render: function () {
    return (
      <Button onClick={this.handleToggle} bsStyle="primary">&#43;</Button>
    );
  },

 
  renderOverlay: function () {
    if (!this.state.isModalOpen) {
      return <span/>;
    }

    return (
        <Modal  onRequestHide={this.handleToggle}>
		  <div className="arrow-tooltip"></div>	
		  <div className="head-project-wraper">
                <h2 className="title-container-project">Name your project..</h2>
          </div> 
          <div className="modal-body">
             <textarea className="project-input" placeholder="Type description.."></textarea>
          </div>
          <div className="modal-footer">
           	 <input type="text" className="input-date datetimepicker" placeholder="Start time.." />
             <input type="text" className="input-date datetimepicker" placeholder="End time.." />      
             <input type="submit" value="create" className="create-button" />
          </div>
        </Modal>
      );
  }
});


//container
var Container = React.createClass({
  render: function() {
    return (
      <div className="container-fluid add-padding">
			<Row>
				<Col lg={5}>
	  				<Logo />
				</Col>
				<Col lg={2} className="centering">
	  				< CustomModalTrigger/>
				</Col>
				<Col lg={5}>
	  				<UserImage />
				</Col>
			</Row>
      </div>
    );
  }
});

//header
var GreenHeader = React.createClass({
  render: function() {
    return (
      <div className="greenheader">
       <Container />
      </div>
    );
  }
});

//stopwatch function javascript 
var Time_track = React.createClass({
	handleStart: function() {
		timer();
  	},
	handleStop: function() {
		stopButton();
  	},
	handleClear: function() {
		 clearButton();		
  	},
  render: function() {
	var Start = this.handleStart;
	var Stop = this.handleStop;
	var Clear = this.handleClear;  
    return (
     <div className="stopwatch" id="stopwatch">
    	<h1 id="minute">00:00:00</h1>
			<div className="wraper_minute">
				<div className="inner_wraper_timer">
					<button onClick={Clear} id="clear" className="repeat_button"></button>
					<button onClick={Start} id="start" className="button_start">&#9658;</button>
					<button onClick={Stop} id="stop" className="button_stop glyphicon glyphicon-stop"></button>
				</div>
			</div>
	</div>
    );
  }
});

//sidebar
var Sidebar = React.createClass({
  render: function() {
    return (
	  
      <div className="sidebar">
	  	 <Time_track />	
	  	 <h2>Navigation</h2>
       	 <ul>
			<li><Link to="Dashboard">Dashboard</Link></li>
            <li><Link to="team">Inbox</Link></li>           
          </ul>		   
      </div>
	  
    );
  }
});

// main component============================
var Mainsection = React.createClass({
  render: function() {
    return (
      <div className="main-section">
       	 <div className="container-fluid no-padding">
			<Row>
				<Col lg={2} className="no-padding">
	  				<Sidebar />
				</Col>
				<Col lg={10} className="no-padding Grey">
					<div className="right-section">
						<RouteHandler/>
					</div>
				</Col>
			</Row>
      </div>
      </div>
    );
  }
});

//handler page dashboard --ini untuk isi halaman dashboard yang isinya list project
var Dashboard = React.createClass({
  render: function() {
    return (
      <div className="dashboard">
       	<ul>
			<li>
				<Link to="project_detail"> <Project_id /> </Link>
			</li>
			<li>
				<Project_id />
			</li>
			<li>
				<Project_id />
			</li>
		</ul>
      </div>
    );
  }
});


// body component============================
var MainBody = React.createClass({
  render: function() {
    return (
      <div className="main-body">
       	<GreenHeader />
		<Mainsection />
      </div>
    );
  }
});

// team page (belum ada)
var team = React.createClass({
  render: function () {
    return (
      <div>
       dsfsd
      </div>
    );
  }
});

//detail project (yang ini untuk  detail project)
var  Show_project_detail = React.createClass({
  render: function () {
    return (
     	
			 <div className="head-project">
                    <h2>Projects - <Link to="Dashboard">Back</Link></h2>
                    <div className="head-left">
                        <h3 className="title-project">unnamed project</h3>
                        <h4 className="detail-project">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat.</h4>
                    </div>
                    <div className="head-right">
                        <h2>Estimated time</h2>
                        <h4>3 Days</h4>
                    </div>
              </div>
		
    );
  }
});

//add task (yang ini pake jquery buat add new task)
var  Add_task = React.createClass({
  render: function () {
    return (
     	 <div id="jtrack-holder">
			<div id="jtrack-content">
				<div id="jtrack-form-list" className="jtrack-form"></div>
				<div id="jtrack-form-create" className="jtrack-form">
					<p>
						<label for="jtrack-task-name">Task name</label>
						<input type="text" name="jtrack-task-name" id="jtrack-task-name" className="jtrack-text" />
					</p>
					<p>
						<label for="jtrack-task-estimate">Estimate</label>
						<input type="text" name="jtrack-task-estimate" id="jtrack-task-estimate" className="jtrack-text" />
					</p>
					<p>
						<input type="button" id="jtrack-button-create" value="Save" />
						<a href="#" className="jtrack-cancel" rel="jtrack-form-create">Cancel</a>
					</p>
					<p  id="jtrack-create-status"></p>
				</div>
				<div id="jtrack-form-update" className="jtrack-form" >
				<p>
						<label for="jtrack-task-name">Task name</label>
						<input type="text" name="jtrack-task-name" id="jtrack-task-name" className="jtrack-text" />
					</p>
					<p>
						<label for="jtrack-task-estimate">Estimate</label>
						<input type="text" name="jtrack-task-estimate" id="jtrack-task-estimate" className="jtrack-text" />
					</p>
					<p>
						<input type="checkbox" name="jtrack-task-completed" id="jtrack-task-completed" /> Task is completed<br />
						<input type="checkbox" name="jtrack-task-archived" id="jtrack-task-archived" /> Task is archived
					</p>
					<p>
						<label>Created at</label>
						<span id="created"></span>
					</p>
					<p>
						<input type="button" id="jtrack-button-update" value="Update" />
						<a href="#" className="jtrack-cancel" rel="jtrack-form-update">Cancel</a>
					</p>
					<p  id="jtrack-update-status"></p>
				</div>
				<div id="jtrack-form-remove" className="jtrack-form" >
					<p id="jtrack-remove-confirm"></p>
					<p>
						<input type="button" id="jtrack-button-remove" value="Delete" />
						<a href="#" className="jtrack-cancel" rel="jtrack-form-remove">Cancel</a>
					</p>
				</div>
				<div id="jtrack-form-remove-all" className="jtrack-form" >
					<p>Are you sure you want to delete all tasks?</p>
					<p>
						<input type="button" id="jtrack-button-remove-all" value="Delete all" />
						<a href="#" className="jtrack-cancel" rel="jtrack-form-remove-all">Cancel</a>
					</p>
				</div>
			</div>
			<div id="jtrack-bar">
				<a href="#" className="jtrack-create"> + New task</a> 				
			</div>
		</div>
    );
  }
});


//detail and add project (yang ini untuk SEMUA detail project DAN ADD)
var  project_detail = React.createClass({
  render: function () {
    return (
     	<div>
			<Show_project_detail />
			<Add_task />
		</div>
    );
  }
});



var routes = (
  	<Route name="MainBody" path="/" handler={MainBody}>
    <Route name="team" handler={team}/>
    <Route name="Dashboard" handler={Dashboard}/>
	<Route name="project_detail" handler={project_detail}/>
    <DefaultRoute handler={Dashboard}/>
  </Route>
);

ReactRouter.run(routes, function (Handler) {
  React.render(<Handler/>, mountNode );
});


